#include "reverse_matrix_hand_vectorization.h"
#include "reverse_matrix_no_optimization.h"
#include <malloc.h>
#include <stdio.h>

#define N 2048
#define M 10

int main() {
/*  int n, m;
  freopen("../in.txt", "r", stdin);
  scanf("%d", &n);
  scanf("%d", &m);*/
  float *matrix = create_matrix(N);
  //float *matrix = create_matrix(n);
  if (matrix == NULL) {
    return 1;
  }
  float *result = create_matrix(N);
  //float *result = create_matrix(n);
  if (result == NULL) {
    free(matrix);
    return 1;
  }

  /*for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      scanf("%f", &matrix[i * n + j]);
    }
  }*/
  random_matrix(matrix, N);

  //  printf("***\n");
  reverse_matrix_no_optimization(matrix, result, N, M);
  //reverse_matrix_no_optimization(matrix, result, n, m);

  // print_matrix(result, n);

  delete_matrix(matrix, N);
  delete_matrix(result, N);
  return 0;
}