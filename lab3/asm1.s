.LC1:
        .string "Enter the accuracy 'N' to calculate ln2"
.LC2:
        .string "sync"
.LC4:
        .string "%.10f\n"
main:
        push    rbx
        sub     rsp, 16
        cmp     edi, 1
        jle     .L2
        mov     rbx, rsi
        mov     rax, QWORD PTR [rsi+8]
        cmp     BYTE PTR [rax], 104
        je      .L2
        mov     edi, OFFSET FLAT:.LC2
        call    system
        mov     rdi, QWORD PTR [rbx+8]
        mov     edx, 10
        mov     esi, 0
        call    strtol
        test    rax, rax
        je      .L13
        pxor    xmm7, xmm7
        movsd   QWORD PTR [rsp+8], xmm7
        mov     edx, 1
        movsd   xmm2, QWORD PTR .LC3[rip]
        movapd  xmm1, xmm2
        jmp     .L12
.L2:
        mov     edi, OFFSET FLAT:.LC1
        call    puts
        jmp     .L4
.L7:
        mov     rcx, rdx
        shr     rcx
        mov     rsi, rdx
        and     esi, 1
        or      rcx, rsi
        pxor    xmm0, xmm0
        cvtsi2sd        xmm0, rcx
        addsd   xmm0, xmm0
.L8:
        movapd  xmm3, xmm1
        divsd   xmm3, xmm0
        movapd  xmm0, xmm3
        addsd   xmm0, QWORD PTR [rsp+8]
        movsd   QWORD PTR [rsp+8], xmm0
.L9:
        add     rdx, 1
        cmp     rax, rdx
        jb      .L5
.L12:
        test    dl, 1
        je      .L6
        test    rdx, rdx
        js      .L7
        pxor    xmm0, xmm0
        cvtsi2sd        xmm0, rdx
        jmp     .L8
.L6:
        test    rdx, rdx
        js      .L10
        pxor    xmm0, xmm0
        cvtsi2sd        xmm0, rdx
.L11:
        movapd  xmm4, xmm2
        divsd   xmm4, xmm0
        movsd   xmm5, QWORD PTR [rsp+8]
        subsd   xmm5, xmm4
        movsd   QWORD PTR [rsp+8], xmm5
        jmp     .L9
.L10:
        mov     rcx, rdx
        shr     rcx
        mov     rsi, rdx
        and     esi, 1
        or      rcx, rsi
        pxor    xmm0, xmm0
        cvtsi2sd        xmm0, rcx
        addsd   xmm0, xmm0
        jmp     .L11
.L13:
        pxor    xmm7, xmm7
        movsd   QWORD PTR [rsp+8], xmm7
.L5:
        movsd   xmm0, QWORD PTR [rsp+8]
        mov     edi, OFFSET FLAT:.LC4
        mov     eax, 1
        call    printf
        movsd   xmm0, QWORD PTR [rsp+8]
        mov     edi, OFFSET FLAT:.LC4
        mov     eax, 1
        call    printf
.L4:
        mov     eax, 0
        add     rsp, 16
        pop     rbx
        ret
.LC3:
        .long   0
        .long   1072693248
