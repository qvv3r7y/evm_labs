#include <stdio.h>
#include <stdlib.h>

static double get_ln_2(int n) {
    double exp = 0;
    for (size_t i = 1; i <= n; ++i) {
        if (i % 2 == 1) exp += 1.0 / i;
        else exp -= 1.0 / i;
    }
    return exp;
    
}

int main(int argc, char *argv[]) {
    system("sync");
    int n = strtol(argv[1], NULL, 10);
    double exp = get_ln_2(n);
    printf("%.10f\n", exp);
    return 0;
}
