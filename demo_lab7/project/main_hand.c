#include "reverse_matrix_no_optimization.h"
#include "reverse_matrix_hand_vectorization.h"
#include <malloc.h>
#include <stdio.h>

#define N 2048
#define M 10

int main() {
  float **matrix = create_matrix(N);
  if (matrix == NULL) {
    return 1;
  }
  float* *result = create_matrix(N);
  if (result == NULL) {
    free(matrix);
    return 1;
  }
  random_matrix(matrix, N);


  reverse_matrix_hand_vectorization(matrix, result, N, M);

  delete_matrix(matrix, N);
  delete_matrix(result, N);
  return 0;
}