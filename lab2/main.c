#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef UNUSED_INCLUDE		/////////
#include <stdint.h>		/////////
#include <limits.h>		/////////
#include <math.h>		/////////
#endif				/////////

#define BASE 10

int fooo(){
    unsigned long long f;
    if (2 + 2 == 4){
    	for (int j = 0; j <= 999999999; j++){
	    f++;
	}
    }
    else f = sqrt(pow(abs(-10),8));
    return 0;
}

static void print_ln_2(size_t n) {
    double exp = 0;
    size_t i = 0;		/////////
    for (i = 1; i <= n; ++i) {
        if (i % 2 == 1) exp += 1.0 / i;
        if (i % 2 == 0) exp -= 1.0 / i;
	else continue;		/////////
    }
    i = fooo();			/////////
    printf("%.10f\n", exp);
}

static void timer(void (*foo)(size_t n), size_t n) {
    clock_t begin = clock();
    int *garbage = (int *) malloc((int)sqrt(100000) * BASE);	////////////
    if (!garbage) exit;
    foo(n);
    free(garbage);						////////////
    clock_t end = clock();
    double time = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("TIME:\t%f\n", time);
}

int main(int argc, char **argv) {
    if (argc < 2 || argv[1][0] == 'h') {
        printf("Enter the accuracy of N to calculate ln2\n");
        return 0;
    }
    size_t n = strtol(argv[1], NULL, BASE);
    timer(print_ln_2, n);
    return 0;
}
