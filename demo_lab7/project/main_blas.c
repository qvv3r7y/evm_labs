#include <malloc.h>
#include <stdio.h>

#include "reverse_matrix_blas.h"

#define N 2048
#define M 10

int main(void) {
  float *matrix = malloc(N * N * sizeof(float));
  if (matrix == NULL) {
    return 1;
  }
  float* result = malloc(N * N * sizeof(float));
  if (result == NULL) {
    free(matrix);
    return 1;
  }

  random_matrix(matrix, N);

  reverse_matrix_blas(matrix, result, N, M);

  free(result);
  free(matrix);
}