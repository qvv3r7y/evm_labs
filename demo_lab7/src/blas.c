#include "cblas.h"
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE 1024
#define ACCURACY 10

float max_sum_row(float *matrix, size_t size) {
	size_t i;
	float sums[size];
	for (i = 0; i < size; ++i) {
		sums[i] = cblas_sasum(size, matrix + size * i, 1);
	}
	return sums[cblas_isamax(size, sums, 1)];
}

float max_sum_column(float *matrix, size_t size) {
	size_t i;
	float sums[size];
	for (i = 0; i < size; ++i) {
		sums[i] = cblas_sasum(size, matrix + i, size);
	}
	return sums[cblas_isamax(size, sums, 1)];
}

void unit_mat(float *res, size_t size) {
	memset(res, 0, size * size * sizeof(float));
	for (size_t i = 0; i < size; ++i) {
		res[i * (size + 1)] = 1;
	}
}

int main() {
	const size_t size = SIZE;

	float *A = calloc(sizeof(float), size * size);
	float *B = calloc(sizeof(float), size * size);
	float *mI = calloc(sizeof(float), size * size);
	float *R = calloc(sizeof(float), size * size);
	float *M = calloc(sizeof(float), size * size);
	float *T = calloc(sizeof(float), size * size);

	if ((A && B && M && R && T && mI) == 0) {
		perror("error in calloc");
		return 0;
	}
	float max_row;
	float max_column;

	for (size_t i = 0; i < size; ++i) {
		for (size_t j = 0; j < size; ++j) {
			A[i * size + j] = (float) (rand() % 1000 + 100);
		}
	}

	clock_t start = clock();

	max_row = max_sum_row(A, size);
	max_column = max_sum_column(A, size);

	cblas_sgemm(CblasRowMajor,
	            CblasTrans,
	            CblasNoTrans,
	            size,
	            size,
	            size,
	            1.0 / max_row / max_column,
	            A,
	            size,
	            A,
	            size,
	            0,
	            R,
	            size);

	unit_mat(mI, size);
	cblas_saxpy(size * size, -1.0f, mI, 1, R, 1);
	memcpy(M, R, size * size * sizeof(float));

	for (size_t i = 0; i < ACCURACY; ++i) {
		cblas_saxpy(size * size, 1.0f, M, 1, mI, 1);
		cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1.0f, M, size, R, size, 0, T, size);
		M = T;
	}
	cblas_sgemm(CblasRowMajor, CblasTrans, CblasNoTrans, size, size, size, 1.0f, A, size, mI, size, 0, T, size);

	printf("Time blas: %f sec\n\r", (double) (clock() - start) / CLOCKS_PER_SEC);

	free(A);
	free(B);
	free(M);
	free(T);
	free(mI);
	free(R);

	return 0;
}
