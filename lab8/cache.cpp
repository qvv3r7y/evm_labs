#include <iostream>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <time.h>
#include <math.h>

#define CASH_L1 32
#define CASH_L2 256
#define CASH_L3 3072

#define N_MIN 256                            //  1 Kib  (uint32_t)
#define N_MAX (32 * 1024 * 256)                // 32 Mib  (uint32_t)

#define STEP(i) (pow(2, (uint64_t) logl(i) + 3))
#define QTY_CYCLE 20

void generate_rand_array(uint32_t *data, size_t size);

uint64_t rdtsc() {
	long long lo, hi;
	__asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
	return ((uint64_t) hi << 32) | lo;
}

enum way {
	FORWARD = 0,
	BACK,
	RANDOM
};

class Traversal {
public:
	Traversal(size_t size, way way) : way_(way), size_(size), arr(new uint32_t[size]()){};
	~Traversal() {
		delete[] arr;
	}
	void initial_arr() {
		switch (way_) {
			case FORWARD:
				for (int i = 0; i < size_ - 1; ++i) {
					arr[i] = i + 1;
				}
				break;
			case BACK:
				for (int i = 1; i < size_; ++i) {
					arr[i] = i - 1;
				}
				arr[0] = size_ - 1;
				break;
			case RANDOM:
				generate_rand_array(arr, size_);
				break;
		}
	}
	void go_traversal() {
		size_t end_cycle = 0;
		if (way_ == BACK) end_cycle = size_ - 1;
		for (int i = 0; i < QTY_CYCLE; ++i) {
			size_t k = end_cycle;
			uint64_t start = rdtsc();

			do {
				k = arr[k];
			} while (k != end_cycle);

			uint64_t end = rdtsc();
			clock_sum += (end - start) / size_;
		}
		clock_sum /= QTY_CYCLE;
		std::cout << clock_sum << " clock" << '\n';
	}
private:
	way way_;
	size_t size_;
	uint64_t clock_sum = 0;
	uint32_t *arr = nullptr;
};

void generate_rand_array(uint32_t *data, size_t size) {
	uint32_t *unvisited = new uint32_t[size - 1];
	unsigned int count_unvisited = size - 1;
	for (unsigned int i = 0; i < count_unvisited; ++i) {
		unvisited[i] = i + 1;
	}
	srand(time(NULL));
	unsigned int current = 0;
	while (count_unvisited) {
		unsigned int index_next = rand() % count_unvisited;
		data[current] = unvisited[index_next];
		current = unvisited[index_next];
		unvisited[index_next] = unvisited[count_unvisited - 1];
		count_unvisited--;
	}
	delete [] unvisited;
}

void start(size_t i, way way){
	Traversal traversal(i,way);
	traversal.initial_arr();
	traversal.go_traversal();
	}

int main(int argc, char **argv) {
	static const char *hello = "Enter ./cache front|back|rand";
	if (argc != 2) {
		std::cout << hello << '\n';
		return 0;
	}

	way way;

	switch (argv[1][0]) {
		case 'f':
			way = FORWARD;
			break;
		case 'b':
			way = BACK;
			break;
		case 'r':
			way = RANDOM;
			break;
		default:
			std::cout << hello << '\n';
			return 0;
	}

	start(N_MAX, way); // прогреваем кэш
	start(N_MAX, way); // прогреваем кэш
	start(N_MAX, way); // прогреваем кэш

	printf("***********MEMORY**CACHE**WARMED**UP************\n");

	for (size_t i = N_MIN; i <= N_MAX; i += STEP(i)) {
		printf("***********%lu KiB\n", i / 256);
		start(i, way);
	}

	return 0;
}
