#include <stdio.h>
#include <stdlib.h>

static double get_ln_2(size_t n) {
    double exp = 0;
    for (size_t i = 1; i <= n; ++i) {
        if (i % 2 == 1) exp += 1.0 / i;
        else exp -= 1.0 / i;
    }
    return exp;
    
}

int main(int argc, char *argv[]) {
    if (argc < 2 || argv[1][0] == 'h') {
        printf("Enter the accuracy 'N' to calculate ln2\n");
        return 0;
    }
    system("sync");
    size_t n = strtol(argv[1], NULL, 10);
    double exp = get_ln_2(n);
    printf("%.10f\n", exp);
    return 0;
}
