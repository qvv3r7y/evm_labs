#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static void print_ln_2(size_t n) {
    double exp = 0;
    for (size_t i = 1; i <= n; ++i) {
        if (i % 2 == 1) exp += 1.0 / i;
        else exp -= 1.0 / i;
    }
    printf("%.10f\n", exp);

}

static void timer(void (*foo)(size_t n), size_t n) {
    clock_t begin = clock();
    foo(n);
    clock_t end = clock();
    double time = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("TIME:\t%f\n", time);
}

int main(int argc, char *argv[]) {
    if (argc < 2 || argv[1][0] == 'h') {
        printf("Enter the accuracy of N to calculate ln2\n");
        return 0;
    }
    system("sync");
    size_t n = strtol(argv[1], NULL, 10);
    timer(print_ln_2, n);
    return 0;
}
