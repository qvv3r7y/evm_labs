#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <time.h>
#include <math.h>

#define CACHE_L1 32 * 1024      // 32 KiB
#define CACHE_L2 256 * 1024     // 256 KiB
#define CACHE_L3 3072 * 1024    // 3 MiB

#define QTY_BANKS 32
#define QTY_CYCLE 30
#define min(x, y) ((x) > (y) ? (y) : (x))

uint64_t rdtsc() {
  long long lo, hi;
  __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
  return ((uint64_t)hi << 32) | lo;
}

uint64_t count(uint32_t *arr, size_t size) {  
    uint64_t sum_ticks = ULLONG_MAX;
    size_t k, i;
    
        uint64_t start = rdtsc();
        for (k = 0, i = 0; i < QTY_CYCLE * size; ++i) {
            k = arr[k];
        }
        uint64_t end = rdtsc();
        sum_ticks = min(sum_ticks, (end - start));
    
    if (k == 1) printf("i'm okey\n");
    return sum_ticks / QTY_CYCLE / size;
}
 
int main(int argc, char *argv[]) {
	static const char *hello = "Enter ./banks qty_banks L1|L2|L3 ";
	if (argc != 2) {
		printf("%s\n", hello);
		return 0;
	}

	size_t size = 0;

	switch (argv[1][1]){
		case '3':
			size += CACHE_L3;
		case '2':
			size += 2*CACHE_L2;
		case '1':
			size += 2*CACHE_L1;
	}
 	
 	for (int qty_banks = 1; qty_banks <= QTY_BANKS; ++qty_banks)
 	{
 		printf("***********banks making : %d\n", qty_banks);

 		uint32_t *data = calloc(qty_banks * size, sizeof(uint32_t));
 
    	for (size_t i = 0; i != size; ++i) {
        	for (size_t bank = 0; bank < qty_banks - 1; ++bank) {
            	data[bank * size + i] = (bank * size + i) + size;
        	}
        	data[(qty_banks - 1) * size + i] = (i + 1) % size;
    	}

 		printf("ticks : %lu\n", count(data, size * qty_banks));
 		free(data);
 	}
 
    return 0;
}
