#include <cblas.h>
#include <malloc.h>
#include <stdlib.h>

float abs_float(float x) {
  if (x < 0) {
    x *= -1;
  }
  return x;
}
void one_matrix(float *matrix, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      matrix[i * n + j] = 0;
    }
    matrix[i * n + i] = 1;
  }
}

void transpose_matrix(float *matrix, float *result, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      result[i * n + j] = matrix[j * n + i];
    }
  }
}

void mul_scalar(float *matrix, float scalar, float *result, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      result[i * n + j] = scalar * matrix[i * n + j];
    }
  }
}

// исполняется один раз, смысла векторизовать нет
float max_j(float *matrix, int n) {
  float result = 0;
  float tmp = 0;
  for (int j = 0; j < n; ++j) {
    for (int i = 0; i < n; ++i) {
      tmp += abs_float(matrix[i * n + j]);
    }
    if (tmp > result) {
      result = tmp;
    }
    tmp = 0;
  }
  return result;
}

// исполняется один раз, смысла векторизовать нет
float max_i(float *matrix, int n) {
  float result = 0;
  float tmp = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      tmp += abs_float(matrix[i * n + j]);
    }
    if (tmp > result) {
      result = tmp;
    }
    tmp = 0;
  }
  return result;
}

void zero_matrix(float *matrix, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      matrix[i * n + j] = 0;
    }
  }
}

void print_matrix(float *matrix, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      printf("%f ", matrix[i * n + j]);
    }
    printf("\n");
  }
}

void random_matrix(float* m, int n) {
  for (int i = 0 ; i < n; ++i) {
    for (int j = 0; j < n ; ++j) {
      m[i * n + j] = rand();
    }
  }
}


void reverse_matrix_blas(float *matrix, float *result, int n, int m) {
  float *b = malloc(n * n * sizeof(float));
  float *tmp1 = malloc(n * n * sizeof(float));
  one_matrix(tmp1, n);
  float a = 1 / (max_j(matrix, n) * max_i(matrix, n));

  cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, n, n, a, tmp1, n,
              matrix, n, 0, b, n); // вычислили B



  float *r = malloc(n * n * sizeof(float));

  one_matrix(r, n);
  cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, -1, b, n,
              matrix, n, 1, r, n); // вычислили R


  float *tmp2 = malloc(n * n * sizeof(float));
  zero_matrix(tmp2, n);
  float *one = malloc(n * n * sizeof(float));
  one_matrix(one, n);
  float *tmp3 = malloc(n * n * sizeof(float));

  for (int i = 0; i < m; ++i) { // tmp1 = R^n, tmp2 = I + R^1 + ...
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1, one, n,
                tmp1, n, 1, tmp2, n);
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1, tmp1, n,
                r, n, 0, tmp3, n);
    /*for (int i = 0 ; i < n; ++i) {
      for (int j = 0 ; j < n; ++j) {
        tmp1[i * n + j] = tmp3[i * n + j];
      }
    }*/
    cblas_scopy(n * n, tmp3, 1, tmp1, 1);
  }

  cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1, tmp2, n, b,
              n, 0, result, n);

  free(r);
  free(tmp1);
  free(tmp2);
  free(tmp3);
  free(one);
  free(b);
}
// С = αAB + βC

// b = a * matrix^T
// C = b
// α = a
// β = 0
// A = I
// B = matrix

// r = -1 * b * matrix + 1*r
// C = r
// α = -1
// β = 1
// A = b
// B = matrix

// tmp2 = tmp1 + tmp2
// C = tmp2
// α = 1
// β = 1
// A = one
// B = tmp1

// tmp1 = tmp1 * r
// A = tmp1
// B = r
// C = rmp1
// α = 1
// β = 0

// result = tmp2 * b
// C = result
// α = 1
// β = 0
// A = tmp2
// B = b