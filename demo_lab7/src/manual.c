#include <time.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <xmmintrin.h>

#define SIZE 1024
#define ACCURACY 10

#define MAX(x, y) ((x) > (y) ? (x) : (y))

float max_sum_row(const float *matrix, size_t size) {
	float max_sum = 0.0f;
	for (size_t i = 0; i < size; ++i) {
		float sum = 0.0f;
		for (size_t j = 0; j < size; ++j) {
			sum += fabsf(matrix[i * size + j]);
		}
		max_sum = MAX(sum, max_sum);
	}
	return max_sum;
}

void transpose(float *transposed, const float *matrix, size_t size) {
	for (size_t i = 0; i < size; ++i) {
		for (size_t j = 0; j < size; ++j) {
			transposed[j * size + i] = matrix[i * size + j];
		}
	}
}

void mul_float(float *res, float *A, float num, size_t size) {
	__m128 mm_num = _mm_load1_ps(&num);
	for (size_t i = 0; i < size * size / 4; ++i) {
		((__m128 *) res)[i] = _mm_sub_ps(((__m128 *) A)[i], mm_num);
	}
}

void mul(float *res, float *A, float *B, size_t size) {
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j += 4) {
			__m128 sum = _mm_setzero_ps();
			for (size_t k = 0; k < size; k++) {
				__m128 row = _mm_set1_ps(A[i * size + k]);
				__m128 column = _mm_load_ps(&B[k * size + j]);
				sum = _mm_add_ps(sum, (_mm_mul_ps(row, column)));
			}
			_mm_store_ps(&res[i * size + j], sum);
		}
	}
}

void unit_matrix(float *res, size_t size) {
	size_t i;
	memset(res, 0, size * size * sizeof(float));
	for (i = 0; i < size; ++i) {
		res[i * (size + 1)] = 1;
	}
}

void add(float *res, float *A, float *B, size_t size) {
	for (size_t i = 0; i < size * size / 4; ++i) {
		((__m128 *) res)[i] = _mm_add_ps(((__m128 *) A)[i], ((__m128 *) B)[i]);
	}
}

void sub(float *res, float *A, float *B, size_t size) {
	for (size_t i = 0; i < size * size / 4; ++i) {
		((__m128 *) res)[i] = _mm_sub_ps(((__m128 *) A)[i], ((__m128 *) B)[i]);
	}
}

int main() {
	const size_t size = SIZE;

	float *A = _mm_malloc(size * size * sizeof(float), 16);
	float *B = _mm_malloc(size * size * sizeof(float), 16);
	float *I = _mm_malloc(size * size * sizeof(float), 16);
	float *R = _mm_malloc(size * size * sizeof(float), 16);

	if ((A && B && I && R) == 0) {
		perror("error in calloc");
		return 0;
	}

	float max_row = 0;
	float max_column = 0;

	for (size_t i = 0; i < size; ++i) {
		for (size_t j = 0; j < size; ++j) {
			A[i * size + j] = (float) (rand() % 1000 + 100);
		}
	}

	clock_t start = clock();

	max_row = max_sum_row(A, size);
	transpose(B, A, size);
	max_column = max_sum_row(B, size);
	mul_float(B, B, 1.0f / (max_row * max_column), size);

	mul(R, B, A, size);
	unit_matrix(I, size);
	sub(R, I, R, size);

	add(A, I, R, size);                              /// A = RES
	memcpy(I, R, size * size * sizeof(float));  /// I = R

	for (size_t i = 0; i < ACCURACY; ++i) {
		mul(I, I, R, size);
		add(A, A, I, size);
	}
	mul(A, A, B, size);

	printf("Time manual: %f sec\n\r", (double) (clock() - start) / CLOCKS_PER_SEC);

	_mm_free(A);
	_mm_free(B);
	_mm_free(I);
	_mm_free(R);

	return 0;
}
/// 21.52 sec