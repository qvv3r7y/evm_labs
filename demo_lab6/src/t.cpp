#include <iostream>
#include <libusb-1.0/libusb.h>
using namespace std;

#define LENGTH_BUFF 100

void printdev(libusb_device *dev);

int main() {
  libusb_device **devs; // можно сказать, что это указатель на указатель устройств. Нельзя работать, но можно получить информацию об устройствах
  libusb_context *ctx = NULL;
  // Структура, представляющая собой сеанс ilbusb.
  // Нужно для того, чтобы программа использовать две библиотеки параллельно
  // Это предотвратит вмешательство между пользователми
  // Сессия создаётся при помощи libusb_init()
  // Закрывается при помощи libusb_exit()
  // Например libusb_exit() не уничтожит данные используемые другим пользователем
  int r;                      // для возвращаемых значений
  ssize_t cnt;                // количество устройств
  r = libusb_init(&ctx);      // Начинаем сессию
  if (r < 0) {    // Проверяем на ошибки
    cout << "Init Error " << r << endl;
    return 1;
  }
  libusb_set_debug(ctx, 3); // Уровень детализации логирования установим на третий уровень, как рекомендовано в документации
  cnt = libusb_get_device_list(ctx, &devs); // Получим список устройств, подключенных к системе в данный момент,
  // Необходимо также при помощи libusb_free_device_list() освобождать список
  if (cnt < 0) { // Проверка на ошибки
    cout << "Get Device Error" << endl;
    libusb_exit(ctx);
    return 1;
  }
  cout << cnt << " Devices in list." << endl;
  for (ssize_t i = 0; i < cnt; i++) {
    cout << endl;
    cout << "Device number: " << i << endl;
    printdev(devs[i]); // print specs of this device
  }
  libusb_free_device_list(devs, 1); // Освободим список устройств
  libusb_exit(ctx);                 // Закончим сессию
  return 0;
}

void printdev(libusb_device *dev) {
  libusb_device_descriptor desc;
  // https://www.usb3.com/whitepapers/USB%203%200%20(11132008)-final.pdf - 9.6.1
  // Стандартный дескриптор устройства USB
  // Описывает общую информацию об устройстве
  // А именно, содержит 18 полей 
  // bLength - размер дескриптора в байтах
  // bDescriptorType - тип дескриптора [всегда равен libusb_descriptor_type::LIBUSB_DT_DEVICE LIBUSB_DT_DEVICE]
  // bcdUSB - номер версии спецификации USB
  // bDeviceClass - класс устройства, http://libusb.sourceforge.net/api-1.0/group__libusb__desc.html#gac529888effbefef8af4f9d17ebc903a1
  // bDeviceSubClass - подкласс устройства. Зависит от bDeviceClass
  // bDeviceProtocol - Протокол взаимодействия. Зависит от прошлых двух полей
  // idVendor - ID изготовителя
  // idProduct - ID продукта
  // bcdDevice - Номер выпуска устройства
  // iManufacturer - Индекс, по которому из строкового дескриптора можно получить описание производителя
  // iProduct - Индекс, по которому из строкового дескриптора можно получить описание продукта
  // iSerialNumber -  Индекс, по которому из строкового дескриптора можно получить серийный номер устройства
  // bNumConfigurations - Количество возможных конфигураций 
  int r = libusb_get_device_descriptor(dev, &desc); // получить дескриптор устройства
  if (r != 0) {
    cout << "failed to get device descriptor" << endl;
    return;
  }


  // Определяем класс устройства, для этого воспользуемся полем bDeviceClass
  cout << "Class: ";
  if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_PER_INTERFACE) {
    cout << "Several classes" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_AUDIO) {
    cout << "Audio class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_COMM) {
    cout << "Communications class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_HID) {
    cout << "Human Interface Device class. " << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_PHYSICAL ) {
    cout << "Physical" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_PRINTER) {
    cout << "Printer class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_PTP) {
    cout << "Image class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_MASS_STORAGE) {
    cout << "Mass storage class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_HUB) {
    cout << "Hub class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_DATA) {
    cout << "Data class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_SMART_CARD) {
    cout << "Smart Card" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_CONTENT_SECURITY) {
    cout << "Content Security" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_VIDEO) {
    cout << "Video" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_PERSONAL_HEALTHCARE) {
    cout << "Personal Healthcare" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_DIAGNOSTIC_DEVICE) {
    cout << "Diagnostic Device" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_WIRELESS) {
    cout << "Wireless class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_APPLICATION ) {
    cout << "Application class" << endl;
  }
  else if (desc.bDeviceClass == libusb_class_code::LIBUSB_CLASS_VENDOR_SPEC) {
    cout << "Class is vendor-specific" << endl;
  }
  else {
    cout << "Unknown class" << endl;
  }
  cout << "Vendor ID: " << desc.idVendor << endl;
  cout << "Product ID: " << desc.idProduct << endl;


  libusb_device_handle* dev_handle;
  // Структура, необходимая для работы с устройством. 
  // Она позволяет выполнять ввод/вывод данных на устройстве
  r = libusb_open(dev, &dev_handle); // Открыть устройство для работы
  if (r == libusb_error::LIBUSB_ERROR_NO_MEM) {
    cerr << "Memory Error" << endl;
  }
  else if (r == libusb_error::LIBUSB_ERROR_ACCESS) {
    cerr << "insufficient permission. Using sudo" << endl;
  }
  else if (r == libusb_error::LIBUSB_ERROR_NO_DEVICE) {
    cerr << "The device has been disconnected" << endl;
  }
  else if (r != 0) {
    cerr << "Error with device" << endl;
    return;
  }

  unsigned char description[LENGTH_BUFF];

  libusb_get_string_descriptor_ascii(dev_handle, desc.iManufacturer, description, LENGTH_BUFF);
  cout << "Manufacturer: " << description << endl;

  libusb_get_string_descriptor_ascii(dev_handle, desc.iProduct, description, LENGTH_BUFF);
  cout << "Product: " << description << endl;

  libusb_get_string_descriptor_ascii(dev_handle, desc.iSerialNumber, description, LENGTH_BUFF);
  cout << "Serial Number: " << description << endl;

  libusb_close(dev_handle);
}

