#include <malloc.h>
#include <stdlib.h>

void print_matrix(float *matrix, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      printf("%.0f ", matrix[i * n + j]);
    }
    printf("\n");
  }
}

float abs_float(float x) {
  if (x < 0) {
    x *= -1;
  }
  return x;
}

float *create_matrix(int n) {
  float *result = malloc(sizeof(float *) * n * n);
  if (result == NULL) {
    return NULL;
  }
  return result;
}

void delete_matrix(float *matrix, int n) {
  /*for (int i = 0; i < n; ++i) {
    free(matrix[i]);
  }*/
  free(matrix);
}

void one_matrix(float *matrix, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      matrix[i * n + j] = 0;
    }
    matrix[i * n + i] = 1;
  }
}

void random_matrix(float *matrix, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      matrix[i * n + j] = (float)rand();
    }
  }
}

/**
 * @param a первый множитель
 * @param b второй множитель
 * @param result матрица, в которую будет записываться результат
 * @param n размер матрицы
 */
void mult_matrix_no_optomizations(float *a, float *b, float *result, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      result[i * n + j] = 0;
    }
  }
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      for (int k = 0; k < n; ++k) {
        result[i * n + j] += a[i * n + k] * b[k * n + j];
      }
    }
  }
}

void sum_matrix_no_optimization(float *a, float *b, float *result, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      result[i * n + j] = b[i * n + j] + a[i * n + j];
    }
  }
}

void init_matrix_no_optimization(float *left_matrix, float *right_matrix,
                                 int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      left_matrix[i * n + j] = right_matrix[i * n + j];
    }
  }
}

void transpose_matrix_no_optimisation(float *matrix, float *result, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      result[i * n + j] = matrix[j * n + i];
    }
  }
}

void mult_scalar_no_optimization(float *matrix, float scalar, float *result,
                                 int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      result[i * n + j] = scalar * matrix[i * n + j];
    }
  }
}

float max_j_no_optimization(float *matrix, int n) {
  float result = 0;
  float tmp = 0;
  for (int j = 0; j < n; ++j) {
    for (int i = 0; i < n; ++i) {
      tmp += abs_float(matrix[i * n + j]);
    }
    if (tmp > result) {
      result = tmp;
    }
    tmp = 0;
  }
  return result;
}

float max_i_no_optimization(float *matrix, int n) {
  float result = 0;
  float tmp = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      tmp += abs_float(matrix[i * n + j]);
    }
    if (tmp > result) {
      result = tmp;
    }
    tmp = 0;
  }
  return result;
}

void reverse_matrix_no_optimization(float *matrix, float *result, int n,
                                    int m) {
  float *tmp1 = create_matrix(n);
  if (tmp1 == NULL) {
    return;
  }
  float *tmp2 = create_matrix(n);
  if (tmp2 == NULL) {
    delete_matrix(tmp1, n);
    return;
  }
  float *b = create_matrix(n);
  if (b == NULL) {
    delete_matrix(tmp1, n);
    delete_matrix(tmp2, n);
    return;
  }
  float *r = create_matrix(n);
  if (r == NULL) {
    delete_matrix(tmp1, n);
    delete_matrix(tmp2, n);
    delete_matrix(b, n);
    return;
  }
  transpose_matrix_no_optimisation(matrix, b, n); // b = matrix^T
  float a1 = max_j_no_optimization(matrix, n);
  float a2 = max_i_no_optimization(matrix, n);
  mult_scalar_no_optimization(b, 1 / a1, b, n);
  mult_scalar_no_optimization(b, 1 / a2, b, n);

  mult_matrix_no_optomizations(b, matrix, tmp1, n); // tmp1 = b * matrix;
  //printf("**\n");
  mult_scalar_no_optimization(tmp1, -1, tmp1, n); // tmp = -b * matrix
  one_matrix(tmp2, n);                            // tmp2 = 1
  sum_matrix_no_optimization(tmp2, tmp1, r, n);   // r = 1 + tmp1

  init_matrix_no_optimization(tmp1, r, n); // tmp1 = r;
  float *tmp3 = create_matrix(n);
  if (tmp3 == NULL) {
    delete_matrix(b, n);
    delete_matrix(r, n);
    delete_matrix(tmp1, n);
    delete_matrix(tmp2, n);
    return;
  }

  //printf("***\n");

  for (int i = 0; i < m; ++i) {
    sum_matrix_no_optimization(tmp2, tmp1, tmp2, n); // tmp2 += tmp1
    mult_matrix_no_optomizations(tmp1, r, tmp3, n);  // tmp3 = tmp1*r
    init_matrix_no_optimization(tmp1, tmp3, n);      // tmp1 *= r
  }
  mult_matrix_no_optomizations(tmp2, b, result, n); // result = tmp2 * b

  delete_matrix(b, n);
  delete_matrix(r, n);
  delete_matrix(tmp1, n);
  delete_matrix(tmp2, n);
  delete_matrix(tmp3, n);
}
