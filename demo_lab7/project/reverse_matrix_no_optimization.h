#ifndef PROJECT_REVERSE_MATRIX_NO_OPTIMIZATION_H
#define PROJECT_REVERSE_MATRIX_NO_OPTIMIZATION_H

float abs_float(float x);

void one_matrix(float *matrix, int n);

float *create_matrix(int n);

void delete_matrix(float *matrix, int n);

void reverse_matrix_no_optimization(float *matrix, float *result, int n, int m);

void print_matrix(float *matrix, int n);

void random_matrix(float *matrix, int n);

#endif // PROJECT_REVERSE_MATRIX_NO_OPTIMIZATION_H
