#include <time.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE 1024
#define ACCURACY 10

#define MAX(x, y) ((x) > (y) ? (x) : (y))

float max_sum_row(const float *matrix, size_t size) {
	float max_sum = 0.0f;
	for (size_t i = 0; i < size; ++i) {
		float sum = 0.0f;
		for (size_t j = 0; j < size; ++j) {
			sum += fabsf(matrix[i * size + j]);
		}
		max_sum = MAX(sum, max_sum);
	}
	return max_sum;
}

void transpose(float *transposed, const float *matrix, size_t size) {
	for (size_t i = 0; i < size; ++i) {
		for (size_t j = 0; j < size; ++j) {
			transposed[j * size + i] = matrix[i * size + j];
		}
	}
}

void mul_float(float *res, const float *matrix, float scalar, size_t size) {
	for (size_t i = 0; i < size; ++i) {
		for (size_t j = 0; j < size; ++j) {
			res[i * size + j] *= scalar;
		}
	}
}

void mul(float *res, const float *a, const float *b, size_t size) {
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			float sum = 0;
			for (size_t k = 0; k < size; k++) {
				sum += a[i * size + k] * b[k * size + j];
			}
			res[i * size + j] = sum;
		}
	}
}

void add(float *res, const float *a, const float *b, size_t size) {
	for (size_t i = 0; i < size; ++i) {
		for (size_t j = 0; j < size; ++j) {
			size_t cur = i * size + j;
			res[cur] = a[cur] + b[cur];
		}
	}
}

void sub(float *res, const float *a, const float *b, size_t size) {
	for (size_t i = 0; i < size; ++i) {
		for (size_t j = 0; j < size; ++j) {
			size_t cur = i * size + j;
			res[cur] = a[cur] - b[cur];
		}
	}
}

void unit_matrix(float *res, size_t size) {
	size_t i;
	memset(res, 0, size * size * sizeof(float));
	for (i = 0; i < size; ++i) {
		res[i * (size + 1)] = 1;
	}
}

void print_matrix(float *matrix, size_t size) {
	size_t i, j;
	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			printf("%f ", matrix[i * size + j]);
		}
		printf("\n");
	}
}

int main() {
	const size_t size = SIZE;
	float *A = calloc(sizeof(float), size * size);
	float *B = calloc(sizeof(float), size * size);
	float *I = calloc(sizeof(float), size * size);
	float *R = calloc(sizeof(float), size * size);
	if ((A && B && I && R) == 0) {
		perror("error in calloc");
		return 0;
	}

	float max_row = 0;
	float max_column = 0;

	for (size_t i = 0; i < size; ++i) {
		for (size_t j = 0; j < size; ++j) {
			A[i * size + j] = (float) (rand() % 1000 + 100);
		}
	}

	clock_t start = clock();

	max_row = max_sum_row(A, size);
	transpose(B, A, size);
	max_column = max_sum_row(B, size);
	mul_float(B, B, 1.0f / (max_row * max_column), size);

	mul(R, B, A, size);
	unit_matrix(I, size);
	sub(R, I, R, size);

	add(A, I, R, size);                              /// A = RES
	memcpy(I, R, size * size * sizeof(float));  /// I = R

	for (size_t i = 0; i < ACCURACY; ++i) {
		mul(I, I, R, size);
		add(A, A, I, size);
	}
	mul(A, A, B, size);

	printf("Time without anything: %f sec\n\r", (double) (clock() - start) / CLOCKS_PER_SEC);

	free(A);
	free(B);
	free(I);
	free(R);

	return 0;
}

/// 84.4sec