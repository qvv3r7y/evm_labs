#include "reverse_matrix_no_optimization.h"
#include <xmmintrin.h>

void mult_matrix_hand_vectorization(float **a, float **b, float **result,
                                    int n) {
  __m128 **rr = (__m128 **)result;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j * 4 < n; ++j) {
      rr[i][j] = _mm_setzero_ps();
    }
  }

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; j += 4) {
      __m128 sum = _mm_setzero_ps();
      for (int k = 0; k < n; ++k) {
        __m128 row_a = _mm_set1_ps(a[i][k]);
        __m128 column_b = _mm_load_ps(&b[k][j]);
        sum += _mm_mul_ps(row_a, column_b);
      }
      _mm_store_ps(&result[i][j], sum);
    }
  }
}

void sum_matrix_hand_vectorization(float **a, float **b, float **result,
                                   int n) {
  for (int i = 0; i < n; ++i) {
    __m128 *rr = (__m128 *)result[i];
    __m128 *aa = (__m128 *)a[i];
    __m128 *bb = (__m128 *)b[i];
    for (int j = 0; j * 4 < n; ++j) {
      rr[j] = _mm_add_ps(aa[j], bb[j]);
    }
  }
}

void init_matrix_hand_vectorization(float **left_matrix, float **right_matrix,
                                    int n) {

  for (int i = 0; i < n; ++i) {
    __m128 *lm = (__m128 *)left_matrix[i];
    __m128 *rm = (__m128 *)right_matrix[i];
    for (int j = 0; j * 4 < n; ++j) {
      lm[j] = rm[j];
    }
  }
}

void transpose_matrix(float **matrix, float **result, int n) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      result[i][j] = matrix[j][i];
    }
  }
}

void mul_scalar(float **matrix, float scalar, float **result, int n) {
  for (int i = 0; i < n; ++i) {
    __m128 *m = (__m128 *)matrix[i];
    __m128 *r = (__m128 *)result[i];
    for (int j = 0; j * 4 < n; ++j) {
      r[j] = scalar * m[j];
    }
  }
}

// исполняется один раз, смысла векторизовать нет
float max_j_hand_vectorization(float **matrix, int n) {
  float result = 0;
  float tmp = 0;
  for (int j = 0; j < n; ++j) {
    for (int i = 0; i < n; ++i) {
      tmp += abs_float(matrix[i][j]);
    }
    if (tmp > result) {
      result = tmp;
    }
    tmp = 0;
  }
  return result;
}

// исполняется один раз, смысла векторизовать нет
float max_i_hand_vectorization(float **matrix, int n) {
  float result = 0;
  float tmp = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      tmp += abs_float(matrix[i][j]);
    }
    if (tmp > result) {
      result = tmp;
    }
    tmp = 0;
  }
  return result;
}

void reverse_matrix_hand_vectorization(float **matrix, float **result, int n,
                                       int m) {
  float **tmp1 = create_matrix(n);
  if (tmp1 == NULL) {
    return;
  }
  float **tmp2 = create_matrix(n);
  if (tmp2 == NULL) {
    delete_matrix(tmp1, n);
    return;
  }
  float **b = create_matrix(n);
  if (b == NULL) {
    delete_matrix(tmp1, n);
    delete_matrix(tmp2, n);
    return;
  }
  float **r = create_matrix(n);
  if (r == NULL) {
    delete_matrix(tmp1, n);
    delete_matrix(tmp2, n);
    delete_matrix(b, n);
    return;
  }
  transpose_matrix(matrix, b, n); // b = matrix^T
  float a1 = max_j_hand_vectorization(matrix, n);
  float a2 = max_i_hand_vectorization(matrix, n);
  mul_scalar(b, 1 / a1, b, n);
  mul_scalar(b, 1 / a2, b, n);

  mult_matrix_hand_vectorization(b, matrix, tmp1, n); // tmp1 = b * matrix;
  mul_scalar(tmp1, -1, tmp1, n);                      // tmp = -b * matrix
  one_matrix(tmp2, n);                                // tmp2 = 1
  sum_matrix_hand_vectorization(tmp2, tmp1, r, n);    // r = 1 + tmp1
  init_matrix_hand_vectorization(tmp1, r, n);         // tmp1 = r;
  float **tmp3 = create_matrix(n);
  if (tmp3 == NULL) {
    delete_matrix(b, n);
    delete_matrix(r, n);
    delete_matrix(tmp1, n);
    delete_matrix(tmp2, n);
    return;
  }

  for (int i = 0; i < m; ++i) {
    sum_matrix_hand_vectorization(tmp2, tmp1, tmp2, n); // tmp2 += tmp1
    mult_matrix_hand_vectorization(tmp1, r, tmp3, n);   // tmp3 = tmp1*r
    init_matrix_hand_vectorization(tmp1, tmp3, n);      // tmp1 *= r
  }
  mult_matrix_hand_vectorization(tmp2, b, result, n); // result = tmp2 * b

  delete_matrix(b, n);
  delete_matrix(r, n);
  delete_matrix(tmp1, n);
  delete_matrix(tmp2, n);
  delete_matrix(tmp3, n);
}
