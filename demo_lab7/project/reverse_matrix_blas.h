#ifndef PROJECT_REVERSE_MATRIX_BLAS_H
#define PROJECT_REVERSE_MATRIX_BLAS_H

void reverse_matrix_blas(float *matrix, float *result, int n, int m);

void random_matrix(float* m, int n);

#endif // PROJECT_REVERSE_MATRIX_BLAS_H
