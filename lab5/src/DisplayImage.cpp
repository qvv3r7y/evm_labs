#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <ctime>

using namespace cv;

static struct timespec start, end;

void draw_window(Mat frame, VideoCapture cap) {
	double fps = 0;
	uint_fast64_t cadre = 0;
	std::string Fps;
	clock_gettime(CLOCK_MONOTONIC_RAW, &start);

	while (true) {
		cadre++;
		cap.read(frame);
		if (frame.empty()) {
			std::cerr << "ERROR! blank frame grabbed\n";
			break;
		}

		Mat frame1, frame2, frame3, frame4, frame5;
		Rect r(125, 75, 250, 200);

		frame.copyTo(frame1);
		frame.copyTo(frame2);
		frame(r).copyTo(frame3);
		frame(r).copyTo(frame5);

		cvtColor(frame, frame1, COLOR_BGR2HSV, 0);
		frame1 = frame1.t();
		for (int i = 0; i < frame1.rows; i++)
			for (int j = 0; j < frame1.cols; j++)
				if ((i % 20 == 10 && j % 2 == 1) ||
					(j % 50 == 25 && i % 2 == 1)) {
					frame1.at<Vec3b>(i, j)[0] = 255;
					frame1.at<Vec3b>(i, j)[1] = 255;
					frame1.at<Vec3b>(i, j)[2] = 255;
				}

		frame2 *= 0.25;

		frame3 += CV_RGB(0, 0, 200);
		frame3 *= 2;

		cvtColor(frame, frame4, COLOR_BGR2GRAY, 0);

		cvtColor(frame, frame5, COLOR_BGR2YCrCb, 0);

		clock_gettime(CLOCK_MONOTONIC_RAW, &end);
		fps = cadre / (end.tv_sec - start.tv_sec + 0.000000001 * (end.tv_nsec - start.tv_nsec));
		std::cout << fps << std::endl;

		imshow("First", frame1);
		imshow("Second", frame2);
		imshow("Third", frame3);
		imshow("Fourth", frame4);
		imshow("Fifth", frame5(r));

		if (waitKey(1) == 's')
			imwrite("screnshoot.jpg", frame3);

		if (waitKey(5) == 27)
			break;
	}
}

int main() {
	Mat frame;
	// INITIALIZE VIDEOCAPTURE
	VideoCapture cap;

	int deviceID = 0;             // 0 = open default camera
	int apiID = cv::CAP_ANY;      // 0 = autodetect default API

	cap.open(deviceID + apiID);

	if (!cap.isOpened()) {
		std::cerr << "ERROR! Unable to open camera\n";
		return -1;
	}

	std::cout << "Start grabbing" << std::endl << "Press any key to terminate" << std::endl;

	draw_window(frame, cap);

	return 0;
}

