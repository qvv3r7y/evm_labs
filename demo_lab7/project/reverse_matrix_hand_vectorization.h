#ifndef PROJECT_REVERSE_MATRIX_HAND_VECTORIZATION_H
#define PROJECT_REVERSE_MATRIX_HAND_VECTORIZATION_H

void reverse_matrix_hand_vectorization(float **matrix, float **result, int n,
                                       int m);

#endif // PROJECT_REVERSE_MATRIX_HAND_VECTORIZATION_H
