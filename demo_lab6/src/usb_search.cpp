#include <libusb-1.0/libusb.h>
#include <cstdio>

using namespace std;

void printDevice(libusb_device *dev, libusb_context *ctx) {


	libusb_handle_events(ctx);
	libusb_device_handle * handle;
	libusb_open(dev,&handle);

	libusb_device_descriptor desc{}; // дескриптор устройства
	libusb_config_descriptor *config; // дескриптор конфигурации объекта
	const libusb_interface *interf;
	const libusb_interface_descriptor *interdesc;
	const libusb_endpoint_descriptor *endpoint_descriptor;
	int r = libusb_get_device_descriptor(dev, &desc);

	unsigned char name[21] = {0};

	libusb_get_string_descriptor_ascii(handle,desc.iManufacturer,name,20);

	if (r < 0) {
		fprintf(stderr, "Descriptor device Err: %d.\n", r);
		return;
	}
	// получить конфигурацию устройства
	printf("^^^^^^^^^^%s", name);
	libusb_get_config_descriptor(dev, 0, &config);
	printf("\n*****************************************************************************\n");
	printf("*| Количество возможных конфигураций: %.2d\n", (int) desc.bNumConfigurations);
	printf("*| Класс устройства: %.2d\n", (int) desc.bDeviceClass);
	printf("*| Идентификатор производителя: %.4d\n", desc.idVendor);
	printf("*| Идентификатор устройства: %.4d\n", desc.idProduct);
	printf("*| Количество интерфейсов: %.3d\n", (int) config->bNumInterfaces);

	for (int i = 0; i < (int) config->bNumInterfaces; i++) {
		interf = &config->interface[i];
		printf("***| Количество альтернативных настроек: %.2d\n", interf->num_altsetting);
		printf("***| Класс устройства: %.2d\n", (int) desc.bDeviceClass);

		for (int j = 0; j < interf->num_altsetting; j++) {
			interdesc = &interf->altsetting[j];
			printf("*****| Номер интерфейса: %.2d\n", (int) interdesc->bInterfaceNumber);
			printf("*****| Количество конечных точек: %.2d\n", (int) interdesc->bNumEndpoints);

			for (int k = 0; k < (int) interdesc->bNumEndpoints; k++) {
				endpoint_descriptor = &interdesc->endpoint[k];
				printf("*******| Тип дескриптора %.2d\n",(int) endpoint_descriptor->bDescriptorType);
				printf("*******| Адрес конечной точки: %d\n",(int) endpoint_descriptor->bEndpointAddress);
			}
		}
	}
	libusb_free_config_descriptor(config);
}

int main() {
	libusb_device **device;
	libusb_context *ctx = nullptr;
	int ret_code;
	ssize_t count_usb;
	ssize_t i;

	ret_code = libusb_init(&ctx);
	if (ret_code < 0) {
		fprintf(stderr, "Initial Err: %d.\n", ret_code);
		return 1;
	}
	// уровень подробности отладочных сообщений
	libusb_set_debug(ctx, 3);

	count_usb = libusb_get_device_list(ctx, &device);
	if (count_usb < 0) {
		fprintf(stderr, "Get_device Err: %d\n", ret_code);
		return 1;
	}

	printf("Количество устройств: %zd\n", count_usb);
	for (i = 0; i < count_usb; i++) { // цикл перебора всех устройств
		printDevice(device[i], ctx); // печать параметров устройства
	}
	printf("************************************************************************\n");

	libusb_free_device_list(device, 1);
	libusb_exit(ctx);
	return 0;
}
