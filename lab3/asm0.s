get_ln_2(unsigned long):
        push    rbp
        mov     rbp, rsp
        mov     QWORD PTR [rbp-24], rdi
        pxor    xmm0, xmm0
        movsd   QWORD PTR [rbp-8], xmm0
        mov     QWORD PTR [rbp-16], 1
.L9:
        mov     rax, QWORD PTR [rbp-16]
        cmp     rax, QWORD PTR [rbp-24]
        ja      .L2
        mov     rax, QWORD PTR [rbp-16]
        and     eax, 1
        test    rax, rax
        je      .L3
        mov     rax, QWORD PTR [rbp-16]
        test    rax, rax
        js      .L4
        cvtsi2sd        xmm0, rax
        jmp     .L5
.L4:
        mov     rdx, rax
        shr     rdx
        and     eax, 1
        or      rdx, rax
        cvtsi2sd        xmm0, rdx
        addsd   xmm0, xmm0
.L5:
        movsd   xmm1, QWORD PTR .LC1[rip]
        divsd   xmm1, xmm0
        movapd  xmm0, xmm1
        movsd   xmm1, QWORD PTR [rbp-8]
        addsd   xmm0, xmm1
        movsd   QWORD PTR [rbp-8], xmm0
        jmp     .L6
.L3:
        mov     rax, QWORD PTR [rbp-16]
        test    rax, rax
        js      .L7
        cvtsi2sd        xmm0, rax
        jmp     .L8
.L7:
        mov     rdx, rax
        shr     rdx
        and     eax, 1
        or      rdx, rax
        cvtsi2sd        xmm0, rdx
        addsd   xmm0, xmm0
.L8:
        movsd   xmm1, QWORD PTR .LC1[rip]
        divsd   xmm1, xmm0
        movsd   xmm0, QWORD PTR [rbp-8]
        subsd   xmm0, xmm1
        movsd   QWORD PTR [rbp-8], xmm0
.L6:
        add     QWORD PTR [rbp-16], 1
        jmp     .L9
.L2:
        movsd   xmm0, QWORD PTR [rbp-8]
        pop     rbp
        ret
.LC2:
        .string "Enter the accuracy 'N' to calculate ln2"
.LC3:
        .string "sync"
.LC4:
        .string "%.10f\n"
main:
        push    rbp
        mov     rbp, rsp
        sub     rsp, 32
        mov     DWORD PTR [rbp-20], edi
        mov     QWORD PTR [rbp-32], rsi
        cmp     DWORD PTR [rbp-20], 1
        jle     .L12
        mov     rax, QWORD PTR [rbp-32]
        add     rax, 8
        mov     rax, QWORD PTR [rax]
        movzx   eax, BYTE PTR [rax]
        cmp     al, 104
        jne     .L13
.L12:
        mov     edi, OFFSET FLAT:.LC2
        call    puts
        mov     eax, 0
        jmp     .L14
.L13:
        mov     edi, OFFSET FLAT:.LC3
        call    system
        mov     rax, QWORD PTR [rbp-32]
        add     rax, 8
        mov     rax, QWORD PTR [rax]
        mov     edx, 10
        mov     esi, 0
        mov     rdi, rax
        call    strtol
        mov     QWORD PTR [rbp-8], rax
        mov     rax, QWORD PTR [rbp-8]
        mov     rdi, rax
        call    get_ln_2(unsigned long)
        movq    rax, xmm0
        mov     QWORD PTR [rbp-16], rax
        mov     rax, QWORD PTR [rbp-16]
        movq    xmm0, rax
        mov     edi, OFFSET FLAT:.LC4
        mov     eax, 1
        call    printf
        mov     eax, 0
.L14:
        leave
        ret
.LC1:
        .long   0
        .long   1072693248
