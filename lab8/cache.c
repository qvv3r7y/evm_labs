#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <time.h>
#include <math.h>

#define CASH_L1 32 
#define CASH_L2 256 
#define CASH_L3 3072 

#define N_MIN 256						    //  1 Kib  (uint32_t)
#define N_MAX (32 * 1024 * 256)    			// 32 Mib  (uint32_t)

#define STEP(i) (pow(2, (uint64_t) logl(i) + 3)) 
#define QTY_CYCLE 20

uint64_t rdtsc() {
  long long lo, hi;
  __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
  return ((uint64_t)hi << 32) | lo;
}

void forward_traversal(size_t size){
	uint64_t clock_sum = 0;
	uint32_t *arr = calloc(size, 4);
	if (!arr){
		printf("Calloc ERR\n");
		return;
	}

	for (int i = 0; i < size - 1; ++i)
	{
		arr[i] = i + 1;
	}

	for (int i = 0; i < QTY_CYCLE; ++i)
	{
		size_t k = 0;
		uint64_t start = rdtsc();

		do {
     		k = arr[k];
		} while (k != 0);

		uint64_t end = rdtsc();
		clock_sum += (end - start) / size;
	}
	clock_sum /= QTY_CYCLE;
	printf("%lu clock\n", clock_sum);
	free(arr);
}


void back_traversal(size_t size){
	uint64_t clock_sum = 0;
	uint32_t *arr = calloc(size, 4);
	if (!arr){
		printf("Calloc ERR\n");
		return;
	}

	for (int i = 1; i < size; ++i)
	{
		arr[i] = i - 1;
	}
	arr[0] = size - 1;

	for (int i = 0; i < QTY_CYCLE; ++i)
	{
		size_t k = size - 1;
		uint64_t start = rdtsc();

		do {
     		k = arr[k];
		} while (k != size - 1);

		uint64_t end = rdtsc();
		clock_sum += (end - start) / size;
	}
	clock_sum /= QTY_CYCLE;
	printf("%lu clock\n", clock_sum);
	free(arr);
}

void generate_rand_array(uint32_t *data, size_t size){
  int* unvisited = malloc(sizeof(int) * (size - 1));
  unsigned int count_unvisited = size - 1;
  for (unsigned int i = 0; i < count_unvisited; ++i) {
    unvisited[i] = i + 1;
  }
  srand(time(NULL));
  unsigned int current = 0;
  while (count_unvisited) {
    unsigned int index_next = rand() % count_unvisited;
    data[current] = unvisited[index_next];
    current = unvisited[index_next];
    unvisited[index_next] = unvisited[count_unvisited - 1];
    count_unvisited--;
  }
  free(unvisited);
}

void rand_traversal(size_t size){
	uint64_t clock_sum = 0;
	uint32_t *arr = calloc(size, 4);
	if (!arr){
		printf("Calloc ERR\n");
		return;
	}

	generate_rand_array(arr, size);

	for (int i = 0; i < QTY_CYCLE; ++i)
	{
		size_t k = 0;
		uint64_t start = rdtsc();

		do {
     		k = arr[k];
		} while (k != 0);

		uint64_t end = rdtsc();
		clock_sum += (end - start) / size;
	}
	clock_sum /= QTY_CYCLE;
	printf("%lu clock\n", clock_sum);
	free(arr);
}



int main(int argc, char **argv){
	static const char *hello = "Err. Enter ./cache front|back|rand";
	if (argc != 2){
		printf("%s\n", hello);
		return 0;
	}

	void (*array_traversal) (size_t);

	switch (argv[1][0]) {
		case 'f':
			array_traversal = forward_traversal;
			break;
		case 'b':
			array_traversal = back_traversal;
			break;
		case 'r':
			array_traversal = rand_traversal;
			break;
		default:
			printf("%s\n", hello);
			return 0;
	}

	array_traversal(N_MAX); // прогреваем кэш
	array_traversal(N_MAX); // прогреваем кэш

	printf("***********MEMORY**CACHE**WARMED**UP************\n");

	for (size_t i = N_MIN; i <= N_MAX; i += STEP(i))
			{
				printf("***********%lu KiB\n",i / 256);
				array_traversal(i);
			}

	return 0;
}
