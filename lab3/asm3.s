.LC1:
        .string "Enter the accuracy 'N' to calculate ln2"
.LC2:
        .string "sync"
.LC4:
        .string "%.10f\n"
main:
        push    rbx
        sub     rsp, 16
        cmp     edi, 1
        jle     .L2
        mov     rax, QWORD PTR [rsi+8]
        mov     rbx, rsi
        cmp     BYTE PTR [rax], 104
        je      .L2
        mov     edi, OFFSET FLAT:.LC2
        call    system
        mov     rdi, QWORD PTR [rbx+8]
        mov     edx, 10
        xor     esi, esi
        call    strtol
        pxor    xmm1, xmm1
        test    rax, rax
        je      .L5
        movsd   xmm2, QWORD PTR .LC3[rip]
        mov     ecx, 1
        jmp     .L10
.L15:
        add     rcx, 1
        addsd   xmm1, xmm3
        cmp     rax, rcx
        jb      .L5
.L10:
        test    rcx, rcx
        js      .L6
        pxor    xmm0, xmm0
        cvtsi2sd        xmm0, rcx
.L7:
        movapd  xmm3, xmm2
        divsd   xmm3, xmm0
        test    cl, 1
        jne     .L15
        add     rcx, 1
        subsd   xmm1, xmm3
        cmp     rax, rcx
        jnb     .L10
.L5:
        movapd  xmm0, xmm1
        mov     edi, OFFSET FLAT:.LC4
        mov     eax, 1
        movsd   QWORD PTR [rsp+8], xmm1
        call    printf
        movsd   xmm1, QWORD PTR [rsp+8]
        mov     edi, OFFSET FLAT:.LC4
        mov     eax, 1
        movapd  xmm0, xmm1
        call    printf
.L4:
        add     rsp, 16
        xor     eax, eax
        pop     rbx
        ret
.L6:
        mov     rdx, rcx
        mov     rsi, rcx
        pxor    xmm0, xmm0
        shr     rdx
        and     esi, 1
        or      rdx, rsi
        cvtsi2sd        xmm0, rdx
        addsd   xmm0, xmm0
        jmp     .L7
.L2:
        mov     edi, OFFSET FLAT:.LC1
        call    puts
        jmp     .L4
.LC3:
        .long   0
        .long   1072693248
